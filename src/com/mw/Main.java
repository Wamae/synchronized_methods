package com.mw;

public class Main {

    public static void main(String[] args) {
        /*Different references - both threads can call MyObject.foo()*/
        /*MyClass.MyObject obj1 = new MyClass.MyObject();
        MyClass.MyObject obj2 = new MyClass.MyObject();

        MyClass thread1 = new MyClass(obj1,"1");
        MyClass thread2 = new MyClass(obj2,"2");
        thread1.start();
        thread2.start();*/

        /*Same Reference to obj . Only one will be allowed to call foo*/
        MyClass.MyObject obj = new MyClass.MyObject();

        MyClass thread1 = new MyClass(obj,"1");
        MyClass thread2 = new MyClass(obj,"2");
        thread1.start();
        thread2.start();

    }

    private static class MyClass extends Thread{
        private String name;
        private MyObject myObj;

        public MyClass(MyObject obj,String n){
            this.myObj= obj;
            this.name = n;
        }

        public void run(){
            myObj.foo(name);
        }

        public static class MyObject{
            public synchronized void foo(String name){
                try{
                    System.out.println("Thread "+name+".foo(): starting");
                    Thread.sleep(3000);
                    System.out.println("Thread "+name+".foo(): ending");
                }catch(InterruptedException e){
                    System.out.println("Thread "+name+":interrupted.");
                }
            }
        }
    }
}
